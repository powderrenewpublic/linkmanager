
import sys
import threading
from linkmanager.log import LOG
from linkmanager.config import (
    global_config_section, ConfigSection, ConfigError )
#from linkmanager.event import Event

class EventLog(object):
    def __init__(self,loggers=[]):
        self.loggers = loggers

    @classmethod
    def from_config(cls,config):
        loggers = []
        for (name,d) in config.loggers.items():
            if name == "influxdb":
                loggers.append(InfluxDBEventLogger.from_config(d))
        return EventLog(loggers)

    def log(self,events=[]):
        for logger in self.loggers:
            logger.log(events)

    def close(self):
        for logger in self.loggers:
            logger.close()
        return


class InfluxDBEventLogger(threading.Thread):
    def __init__(self,tags=dict(),outputs=dict()):
        super(InfluxDBEventLogger,self).__init__()
        from influxdb import InfluxDBClient
        self.tags = tags
        self.outputs = outputs
        self.clients = dict()
        self.queues = dict()
        self.lock = threading.Lock()
        self.cv = threading.Condition(lock=self.lock)
        self._cancel = False

        for (name,d) in self.outputs.items():
            if "enabled" in d and not d["enabled"]:
                continue
            if name in self.clients:
                raise ConfigError("duplicate influxdb output name %s" % (name,))
            self.clients[name] = InfluxDBClient(
                d.get("hostname",""),d.get("port",""),
                d.get("username",""),d.get("password",""),d.get("database",""),
                ssl=d.get("ssl",False),timeout=2,retries=-1)
            self.queues[name] = []

    @classmethod
    def from_config(cls,config):
        return InfluxDBEventLogger(tags=config.get("tags",dict()),
                                   outputs=config.get("outputs",dict()))

    def log(self,events=[]):
        if not events:
            return
        if not self.is_alive():
            self.start()
        enqueue = dict()
        for name in self.clients:
            points = []
            for e in events:
                tags = dict(self.tags)
                tags["event"] = e.logeventname
                values = dict()
                for (k,v) in e.logtags.items():
                    tags[k] = v
                for (k,v) in e.logvalues.items():
                    values[k] = v
                ms = "%s%s" % (self.outputs[name].get("measurement_prefix",""),
                               e.logname)
                ed = dict(measurement=ms,time=int(e.timestamp*(10**9)),
                          tags=tags,fields=values)
                points.append(ed)
            enqueue[name] = points
        with self.cv:
            for name in enqueue:
                self.queues[name].extend(enqueue[name])
            self.cv.notify()

    def run(self):
        tel = []
        try:
            import urllib3.exceptions
            tel.append(urllib3.exceptions.ReadTimeoutError)
        except:
            pass
        try:
            import requests.exceptions
            tel.append(requests.exceptions.ReadTimeout)
        except:
            pass
        if not tel:
            tel = [Exception]
        tel = tuple(tel)
        while True:
            if self._cancel:
                LOG.debug("stopping influxdb event log thread")
                return
            tosend = dict()
            sent = dict()
            remaining = 0
            self.cv.acquire()
            for name in self.queues.keys():
                if len(self.queues[name]):
                    tosend[name] = min(len(self.queues[name]),32)
                    if len(self.queues[name]) > 32:
                        remaining += len(self.queues[name]) - 32
            self.cv.release()
            if tosend:
                for name in tosend.keys():
                    try:
                        self.clients[name].write_points(self.queues[name][0:tosend[name]])
                        sent[name] = tosend[name]
                        LOG.debug("sent %d points to output %s",tosend[name],name)
                    except tel:
                        LOG.warn("timeout while sending %d points to output %s",
                                  tosend[name],name)
                    except Exception:
                        LOG.error("failed to send %d points to output %s",
                                  tosend[name],name,exc_info=sys.exc_info())
                        remaining += tosend[name]
            if sent:
                self.cv.acquire()
                for name in sent.keys():
                    del self.queues[name][0:sent[name]]
                self.cv.release()
            timeout = None
            if remaining:
                timeout = 8
            with self.cv:
                self.cv.wait(timeout=timeout)

    def close(self):
        LOG.debug("closing influxdb event log")
        self._cancel = True
        with self.cv:
            self.cv.notify()
        self.join()

@global_config_section
class EventLogConfig(ConfigSection):
    name = "eventlog"
    reconfig = False

    # It's up to individual loggers to raise ConfigErrors; they might be
    # out-of-tree modules.
    def __init__(self,configdir,jsonblob={}):
        super(EventLogConfig,self).__init__(configdir,jsonblob)
        self.loggers = jsonblob
