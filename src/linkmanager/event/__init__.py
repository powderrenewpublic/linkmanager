
import time

def dict2str(d):
    ret = ""
    for (k,v) in d.items():
        c = ","
        if ret == "":
            c = ""
        ret += "%r=%r%s" % (k,v,c)
    return ret

event_id_seed = int(time.time())
event_id_next = 1

class Event(object):
    """
    An event is a notification of a state change to some system component.
    """
    def __init__(self,eid=None,timestamp=time.time(),msg=""):
        self.eid = eid
        self.timestamp = timestamp
        self.msg = msg

    @classmethod
    def next_id(cls):
        """
        If user does not provide IDs, we need a way to generate monotonically-icreasing IDs of some sort.  So we start with the load timestamp, and add a nanosecond for each event.  This works for systems with reliable clocks.
        """
        global event_id_next
        ret = event_id_seed + float(event_id_next) / 10**9
        event_id_next += 1
        return ret

    @property
    def logname(self):
        return "event"

    @property
    def logeventname(self):
        return "event"

    @property
    def logtags(self):
        if self.eid is not None:
            return dict(eid=self.eid)
        return dict()

    @property
    def logvalues(self):
        return dict()

    def __repr__(self):
        return "%s(%s,%s,%s,%s,%f,%r)" \
          % (self.__class__.__name__,self.logname,self.logeventname,
             dict2str(self.logtags),dict2str(self.logvalues),
             self.timestamp,self.msg)

class InterfaceEvent(Event):
    """
    An interface event is a notification of a state change to a network link.
    """
    def __init__(self,interface,eid=None,timestamp=time.time(),msg=""):
        super(InterfaceEvent,self).__init__(eid,timestamp,msg)
        self.interface = interface

    @property
    def logname(self):
        return "interface"

    @property
    def logeventname(self):
        return "interface"

    @property
    def logtags(self):
        d = dict(interface=self.interface.name)
        if self.eid is not None:
            d["id"] = self.eid
        return d

    @property
    def logvalues(self):
        return dict(carrier=self.interface.carrier,
                    operstate=self.interface.operstate,
                    address=self.interface.address)

class InterfaceLoadEvent(InterfaceEvent):
    @property
    def logeventname(self):
        return "load"

class InterfaceNewEvent(InterfaceEvent):
    @property
    def logeventname(self):
        return "new"

class InterfaceDelEvent(InterfaceEvent):
    @property
    def logeventname(self):
        return "del"

class InterfaceCarrierEvent(InterfaceEvent):
    @property
    def logevent(self):
        return "carrier"

class InterfaceOperStateEvent(InterfaceEvent):
    @property
    def logeventname(self):
        return "operstate"

class AddressEvent(Event):
    def __init__(self,interface,family,address,prefixlen,
                 eid=None,timestamp=time.time(),msg=""):
        super(AddressEvent,self).__init__(eid,timestamp,msg)
        self.interface = interface
        self.family = family
        self.address = address
        self.prefixlen = prefixlen

    @property
    def logname(self):
        return "address"

    @property
    def logeventname(self):
        return "address"

    @property
    def logtags(self):
        d = dict(interface=self.interface)
        if self.eid is not None:
            d["id"] = self.eid
        return d

    @property
    def logvalues(self):
        return dict(family=self.family,
                    address=self.address,
                    prefixlen=self.prefixlen)

class AddressLoadEvent(AddressEvent):
    @property
    def logeventname(self):
        return "load"

class AddressNewEvent(AddressEvent):
    @property
    def logeventname(self):
        return "new"

class AddressDelEvent(AddressEvent):
    @property
    def logeventname(self):
        return "del"

class RouteEvent(Event):
    def __init__(self,route,eid=None,timestamp=time.time(),msg=""):
        super(RouteEvent,self).__init__(eid,timestamp,msg)
        self.route = route

    @property
    def logname(self):
        return "route"

    @property
    def logeventname(self):
        return "route"

    @property
    def logtags(self):
        d = dict()
        if "interface" in self.route:
            d["interface"] = self.route["interface"]
        if self.eid is not None:
            d["id"] = self.eid
        return d

    @property
    def logvalues(self):
        vd = dict(self.route)
        if "interface" in vd:
            del vd["interface"]
        return vd

class RouteLoadEvent(RouteEvent):
    @property
    def logeventname(self):
        return "load"

class RouteNewEvent(RouteEvent):
    @property
    def logeventname(self):
        return "new"

class RouteDelEvent(RouteEvent):
    @property
    def logeventname(self):
        return "del"

class DefRouteNewEvent(RouteEvent):
    @property
    def logeventname(self):
        return "defroute_new"

class DefRouteUpdateEvent(RouteEvent):
    @property
    def logeventname(self):
        return "defroute_update"

class DefRouteChangeEvent(RouteEvent):
    @property
    def logeventname(self):
        return "defroute_change"

class DefRouteDelEvent(RouteEvent):
    @property
    def logeventname(self):
        return "defroute_del"

class ServiceEvent(Event):
    """
    A service event is a notification of a state change to a service run by the system init.
    """
