
import sys
import socket
import pyroute2
from linkmanager.log import LOG
from linkmanager.util import DictNamespace

class NetlinkMsg(object):
    ATTR_PREFIX = None
    types = DictNamespace(
        RTM_NEWLINK=16,RTM_DELLINK=17,RTM_GETLINK=18,RTM_SETLINK=19,
        RTM_NEWADDR=20,RTM_DELADDR=21,RTM_GETADDR=22,
        RTM_NEWROUTE=24,RTM_DELROUTE=25,RTM_GETROUTE=26,
        RTM_NEWNEIGH=28,RTM_DELNEIGH=29,RTM_GETNEIGH=30,
        RTM_NEWRULE=32,RTM_DELRULE=33,RTM_GETRULE=34,
        RTM_NEWQDISC=36,RTM_DELQDISC=37,RTM_GETQDISC=38,
        RTM_NEWTCLASS=40,RTM_DELTCLASS=41,RTM_GETTCLASS=42,
        RTM_NEWTFILTER=44,RTM_DELTFILTER=45,RTM_GETTFILTER=46,
        RTM_NEWACTION=48,RTM_DELACTION=49,RTM_GETACTION=50,
        RTM_NEWPREFIX=52,
        RTM_GETMULTICAST=58,
        RTM_GETANYCAST=62,
        RTM_NEWNEIGHTBL=64,RTM_GETNEIGHTBL=66,RTM_SETNEIGHTBL=67,
        RTM_NEWNDUSEROPT=68,
        RTM_NEWADDRLABEL=72,RTM_DELADDRLABEL=73,RTM_GETADDRLABEL=74,
        RTM_GETDCB=78,RTM_SETDCB=79,
        RTM_NEWNETCONF=80,RTM_DELNETCONF=81,RTM_GETNETCONF=82,
        RTM_NEWMDB=84,RTM_DELMDB=85,RTM_GETMDB=86,
        RTM_NEWNSID=88,RTM_DELNSID=89,RTM_GETNSID=90,
        RTM_NEWSTATS=92,RTM_GETSTATS=94,
        RTM_NEWCACHEREPORT=96)
    typesByNum = DictNamespace({v:k for k,v in types.items()})
    
    def __init__(self,msg,nltype,length,nlflags,seqno,event):
        self.msg = msg
        self.nltype = nltype
        self.length = length
        self.nlflags = nlflags
        self.seqno = seqno
        self.event = event
        self.attrs = dict()

    def _msgName(self):
        if self.event:
            ts = str(self.event)
        elif self.nltype in NetlinkMsg.typesByNum:
            ts = NetlinkMsg.typesByNum[self.nltype]
        else:
            ts = str(self.nltype)
        return ts

    def attr(self,attr=None):
        if attr in self.attrs:
            return self.attrs[attr]
        if hasattr(self.__class__,"ATTR_PREFIX"):
            prefix = self.__class__.ATTR_PREFIX
            if "%s_%s" % (prefix,attr.upper()) in self.attrs:
                return self.attrs["%s_%s" % (prefix,attr.upper())]
        return None

    def __repr__(self):
        return "NetlinkMsg(%s)" % (self._msgName(),)

    def dumps(self,skipattrslike=[]):
        return "%s(length=%d,nlflags=0x%x,seqno=%d)" % (
            self._msgName(),self.length,self.nlflags,self.seqno)

    @staticmethod
    def parse(m):
        if not isinstance(m,dict):
            raise Exception("expecting dict (got %s)" % (type(m)))
        if 'header' not in m:
            raise Exception("no message header")
        h = m['header']
        t = h['type']
        if t >= NetlinkMsg.types.RTM_NEWLINK \
          and t <= NetlinkMsg.types.RTM_SETLINK:
            return LinkMsg(
                m,t,h['length'],h['flags'],h['sequence_number'],m['event'],
                m['family'],m['ifi_type'],m['index'],m['flags'],m['change'],
                attrs=m['attrs'])
        elif t >= NetlinkMsg.types.RTM_NEWADDR \
          and t <= NetlinkMsg.types.RTM_GETADDR:
            return AddrMsg(
                m,t,h['length'],h['flags'],h['sequence_number'],m['event'],
                m['family'],m['prefixlen'],m['flags'],m['scope'],m['index'],
                attrs=m['attrs'])
        elif t >= NetlinkMsg.types.RTM_NEWROUTE \
          and t <= NetlinkMsg.types.RTM_GETROUTE:
            return RouteMsg(
                m,t,h['length'],h['flags'],h['sequence_number'],m['event'],
                m['family'],m['dst_len'],m['src_len'],m['tos'],m['table'],
                m['proto'],m['scope'],m['type'],m['flags'],
                attrs=m['attrs'])
        return NetlinkMsg(
            m,t,h['length'],h['flags'],h['sequence_number'],m['event'])

class LinkMsg(NetlinkMsg):
    ATTR_PREFIX = "IFLA"

    def __init__(self,msg,nltype,length,nlflags,seqno,event,
                 family,ifi_type,index,flags,change=0,attrs=[]):
        super(LinkMsg,self).__init__(msg,nltype,length,nlflags,seqno,event)
        self.family = family
        self.ifi_type = ifi_type
        self.index = index
        self.flags = flags
        self.change = change
        if isinstance(attrs,list):
            self.attrs = dict()
            for (k,v) in attrs:
                self.attrs[k] = v
        elif isinstance(attrs,dict):
            self.attrs = attrs
        elif attrs is None:
            self.attrs = dict()
        else:
            raise Exception("unknown attrs type (%s)" % (type(attrs)))

    def __repr__(self):
        return "LinkMsg(%s)" % (self._msgName(),)

    def dumps(self,skipattrslike=[]):
        sl = [ "%s(family=%d,type=%d,flags=%x,change=%d)" % (
            self._msgName(),self.family,self.ifi_type,self.flags,
            self.change) ]
        for (k,v) in self.attrs.items():
            ss = False
            for skip in skipattrslike:
                if skip.upper() in k:
                    ss = True
                    break
            if not ss:
                sl.append("  %s = %s" % (k,str(v)))
        return sl

class AddrMsg(NetlinkMsg):
    ATTR_PREFIX = "IFA"

    def __init__(self,msg,nltype,length,nlflags,seqno,event,
                 family,prefixlen,flags,scope,index,
                 attrs=[]):
        super(AddrMsg,self).__init__(msg,nltype,length,nlflags,seqno,event)
        self.family = family
        self.prefixlen = prefixlen
        self.flags = flags
        self.scope = scope
        self.index = index
        if isinstance(attrs,list):
            self.attrs = dict()
            for (k,v) in attrs:
                self.attrs[k] = v
        elif isinstance(attrs,dict):
            self.attrs = attrs
        elif attrs is None:
            self.attrs = dict()
        else:
            raise Exception("unknown attrs type (%s)" % (type(attrs)))

    def __repr__(self):
        return "AddrMsg(%s)" % (self._msgName(),)

    def dumps(self,skipattrslike=[]):
        sl = [ "%s(family=%d,prefixlen=%d,flags=%x,scope=%d,index=%d)" % (
            self._msgName(),self.family,self.prefixlen,self.flags,
            self.scope,self.index) ]
        for (k,v) in self.attrs.items():
            ss = False
            for skip in skipattrslike:
                if skip.upper() in k:
                    ss = True
                    break
            if not ss:
                sl.append("  %s = %s" % (k,str(v)))
        return sl

class RouteMsg(NetlinkMsg):
    ATTR_PREFIX = "RTA"

    def __init__(self,msg,nltype,length,nlflags,seqno,event,
                 family,dst_len,src_len,tos,table,
                 proto,scope,rtm_type,flags,
                 attrs=[]):
        super(RouteMsg,self).__init__(msg,nltype,length,nlflags,seqno,event)
        self.family = family
        self.dst_len = dst_len
        self.src_len = src_len
        self.table = table
        self.proto = proto
        self.scope = scope
        self.rtm_type = rtm_type
        self.flags = flags
        if isinstance(attrs,list):
            self.attrs = dict()
            for (k,v) in attrs:
                self.attrs[k] = v
        elif isinstance(attrs,dict):
            self.attrs = attrs
        elif attrs is None:
            self.attrs = dict()
        else:
            raise Exception("unknown attrs type (%s)" % (type(attrs)))

    def __repr__(self):
        return "RouteMsg(%s)" % (self._msgName(),)

    def toNetlinkDict(self):
        ret = dict()
        oattrs = [ "family","dst_len","src_len","table","proto","scope" ]
        kattrs = [ "dst","src","iif","oif","gateway","priority","prefsrc",
                   "table" ]
        for a in oattrs:
            ret[a] = getattr(self,a)
        for a in kattrs:
            v = self.attr(a)
            if v is not None:
                ret[a] = v
        return ret

    def dumps(self,skipattrslike=[]):
        sl = [ "%s(family=%d,dst_len=%d,src_len=%d,table=%d,proto=%d,scope=%d,type=%d,flags=%x)" % (
            self._msgName(),self.family,self.dst_len,self.src_len,
            self.table,self.proto,self.scope,self.rtm_type,self.flags) ]
        for (k,v) in self.attrs.items():
            ss = False
            for skip in skipattrslike:
                if skip.upper() in k:
                    ss = True
                    break
            if not ss:
                sl.append("  %s = %s" % (k,str(v)))
        return sl

class RtNetlink(object):
    def __init__(self):
        # Get a netlink connection.
        self.rtnl = pyroute2.IPRoute()
        self.rtnlstream = pyroute2.IPRoute()

    def listen(self):
        self.rtnlstream._sock.setsockopt(
            socket.SOL_SOCKET,socket.SO_RCVBUF,4*1024*1024)
        self.rtnlstream.bind()

    def fileno(self):
        return self.rtnlstream._sock.fileno()

    def handler(self):
        ml = self.rtnlstream.get()
        if not ml:
            return []
        return [ NetlinkMsg.parse(m) for m in ml ]

    def get_links(self,**kwargs):
        ret = []
        ml = self.rtnl.get_links(**kwargs)
        for m in ml:
            ret.append(NetlinkMsg.parse(m))
        return ret

    def get_routes(self,**kwargs):
        ret = []
        ml = self.rtnl.get_routes(**kwargs)
        for m in ml:
            ret.append(NetlinkMsg.parse(m))
        return ret
            
    def get_addrs(self,**kwargs):
        ret = []
        ml = self.rtnl.get_addr(**kwargs)
        for m in ml:
            ret.append(NetlinkMsg.parse(m))
        return ret

    def replaceRoute(self,old={},new={},newfields={},delete_first=False):
        """
        Looks for the first route matching all keys in old (at least one
        key must match!).  If it doesn't find one, it simply adds a new
        route.  If it does find one, that route is first removed, then a
        new route is added.
        
        There are two ways to construct the new route.  First, if new is
        None or the empty dict, all key/value pairs in the route
        obtained by old (or just old itself, if no route was found) are
        the basis for the route; otherwise, only the key/value pairs in
        new are the basis for the route.  Once we have the basis, any
        fields in newfields are added to the basis.  The extended basis
        dict key/values are expanding and sent to
        pyroute2.IPRoute.route().
        """
        rl = self.rtnl.get_routes(**old)
        basis = None
        if rl:
            basis = dict(rl[0])
            del basis['event']
            del basis['header']
            attrs = basis['attrs']
            del basis['attrs']
            for (k,v) in attrs:
                if not k.startswith("RTA_"):
                    continue
                k = k[4:].lower()
                basis[k] = v
        if basis and delete_first:
            try:
                ret = self.rtnl.route('del',**basis)
                LOG.debug("replaceRoute: route del (%s): %s",
                          str(**basis),str(ret))
            except pyroute2.NetlinkError:
                LOG.error("replaceRoute failed: route del (%s): %s",
                          str(**basis),str(ret),
                          exc_info=sys.exc_info())
        nbasis = dict(basis)
        if new:
            nbasis = new
        elif not nbasis:
            nbasis = old
        if newfields:
            for (k,v) in newfields:
                nbasis[k] = v
        ret = self.rtnl.route('add',**nbasis)
        LOG.debug("replaceRoute: route add (%s): %s",
                  str(**basis),str(ret))
        if basis and not delete_first:
            try:
                ret = self.rtnl.route('del',**basis)
                LOG.debug("replaceRoute: route del (%s): %s",
                          str(**basis),str(ret))
            except pyroute2.NetlinkError:
                LOG.error("replaceRoute failed: route del (%s): %s",
                          str(**basis),str(ret),
                          exc_info=sys.exc_info())
        return
