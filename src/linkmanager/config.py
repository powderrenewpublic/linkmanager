
import os
import json
import sys
import logging
import importlib
import abc
import six
import socket
from linkmanager.log import LOG
from linkmanager.util import isiterable

DEFAULT_CONFIGFILE_PATHS = [ "/etc/linkmanager/config.json" ]
if os.getenv("HOME"):
    DEFAULT_CONFIGFILE_PATHS.insert(
        0,"%s/.linkmanager/config.json" % (os.getenv("HOME")))
if os.getenv("LINKMANAGER_CONFIGFILE"):
    DEFAULT_CONFIGFILE_PATHS = [ os.getenv("LINKMANAGER_CONFIGFILE") ]

def find_configfile():
    for fp in DEFAULT_CONFIGFILE_PATHS:
        if os.path.exists(fp) and os.access(fp,os.R_OK):
            return fp
    return None

def add_default_configfile(fp):
    global DEFAULT_CONFIGFILE_PATHS
    if not fp in DEFAULT_CONFIGFILE_PATHS:
        DEFAULT_CONFIGFILE_PATHS.insert(0,fp)
    return None

CONFIG_SECTIONS = {}
CONFIG_SECTION_ORDER = []
CONFIG_PLUGIN_SECTIONS = []

def global_config_section(cls):
    """
    A simple decorator that "registers" classes as top-level ConfigSections
    for our Config object.
    """
    global CONFIG_SECTIONS
    csname = cls.name
    if csname in CONFIG_SECTIONS:
        raise RuntimeError("section '%s' already globally registered!" % (csname))
    CONFIG_SECTIONS[csname] = cls
    CONFIG_SECTION_ORDER.append(csname)
    if hasattr(cls,'plugin'):
        CONFIG_PLUGIN_SECTIONS.append(csname)
    return cls

class ConfigError(Exception):
    pass

@six.add_metaclass(abc.ABCMeta)
class ConfigSection(object):
    """
    A ConfigSection processes a chunk of the json config file, and is
    "applied" to an Manager instance, once parsed and validated.
    """
    name = None
    """The json key for this section"""
    reconfig = False
    """True if this section supports online reconfiguration; False otherwise"""

    def __init__(self,configdir,jsonblob={}):
        self.configdir = configdir
        self._json = jsonblob
        self._hash = hash(json.dumps(jsonblob,
                                     sort_keys=True,separators=(',',':')))
        return

    @property
    def hash(self):
        return self._hash

    def __eq__(self,other):
        """
        :returns: True if :param self: is identical to :param other: ; False otherwise.
        """
        return type(self) == type(other) and self.hash == other.hash

    def __ne__(self,other):
        """
        :returns: True if self differs from :param other: ; False otherwise.
        """
        return not self.__eq__(other)

    def apply(self,manager,oldsection=None,oldobjects=()):
        """
        Applies this section to the manager.
        If self.reconfig is True, this method may also be called for
        online reconfiguration of a running manager.  If you 
        want to save the old :param oldobjects:, then instead of
        returning a new object, return the (possibly modified) oldobjects.
        """
        return None

    def dumps(self,full=False,indent=0):
        return repr(self)

class Config(object):
    """
    An object containing any parsed `ConfigSection` objects, as well as
    plugin instance objects that resulted from the parse.

    Each ConfigSection must stand by itself, depending on no other
    ConfigSections.  This restriction is to allow online reconfiguration
    of an existing server, given that we allow some sections to prevent
    their reconfiguration.  For instance, suppose the user edits the
    config file for a running server, and changes the server host/port
    AND adds a new static user.  We want to pick up the new static user,
    but we will not restart the server on a new port.  Thus, in that
    case, we need to be able to graft the old, unmodifiable sections of
    the old config with the modified (or modifiable) sections of the new
    config.  Thus each config section must be independent.
    """
    def __init__(self,configfile=find_configfile(),sections={},order=[]):
        """
        Parses :param configfile:, either according to :param sections:
        and :param order: (although if sections is specified and order
        is not, order will be automatically created as sections.keys()),
        or to the global CONFIG_SECTIONS and CONFIG_SECTION_ORDER vars
        that are updated by the `global_config_section` decorator.  The
        fields resulting from the parsed config are set (by section
        name) in this object.  Any state object returned by
        `section.apply()` is stored as generic state in the server as
        well.
        """
        #if not configfile:
        #    raise ConfigError("must create a config file!")
        self.configfile = configfile
        contents = "{}"
        if self.configfile:
            st = os.stat(self.configfile)
            self.timestamp = st.st_mtime
            fd = open(self.configfile,'r')
            contents = fd.read()
            fd.close()
            self.configdir = os.path.dirname(self.configfile)
        else:
            self.configdir = None
        jsonblob = json.loads(contents)
        if sections:
            self.sections = sections
            self.order = order or self.sections.keys()
        else:
            self.sections = CONFIG_SECTIONS
            self.order = CONFIG_SECTION_ORDER
        
        for section in self.order:
            blob = {}
            if section in jsonblob:
                blob = jsonblob[section]
            if not section in self.sections:
                raise ConfigError("section '%s' specified in ordering, but no class"
                                  " specified to process it!" % (section))
            ns = self.sections[section](self.configdir,blob)
            setattr(self,section,ns)
            LOG.debug("config section: %s",ns.dumps())

    def is_stale(self):
        """Check if the config file has been updated since last parse."""
        if self.configfile:
            st = os.stat(self.configfile)
            if self.timestamp < st.st_mtime:
                return st.st_mtime
        return False

    def reload(self):
        """Reload the config file.  :returns: a new Config instance."""
        return Config(self.configfile,sections=self.sections,order=self.order)

    def apply(self,manager,newconfig=None):
        """
        Applies this Config to the :param manager:.
        """
        if newconfig is not None:
            self.timestamp = newconfig.timestamp
        for section in self.order:
            sc = getattr(self,section)
            if not sc:
                # This is the first time applying this config file to the manager;
                # handle differently.
                raise ConfigError("missing section '%s'" % (str(section)))
            isdiff = False
            newsection = None
            if newconfig:
                newsection = getattr(newconfig,section)
                isdiff = not sc == newsection
            if isdiff:
                if getattr(sc,'reconfig',False):
                    LOG.info("reconfiguring manager with updated section '%s'",
                             section)
                    oso = manager.get_section_objects(section)
                    ret = newsection.apply(manager,oldsection=sc,oldobjects=oso)
                    LOG.info("saving newsection '%s' configuration after update",
                             section)
                    setattr(self,section,newsection)
                    manager.process_section_objects(section,ret)
                else:
                    LOG.warning("modified section '%s' in %s not reconfigurable;"
                                " ignoring!",section,self.configfile)
            elif not newconfig:
                LOG.debug("configuring manager with section '%s'",section)
                ret = sc.apply(manager)
                manager.process_section_objects(section,ret)
        return 0

    def dump(self,full=False):
        retval = ""
        for section in self.order:
            if hasattr(self,section):
                st = getattr(self,section).dumps(indent=4)
                retval += "  Section '%s':\n    %s\n" % (section,st)
        return retval

@global_config_section
class PluginConfig(ConfigSection):
    name = "plugins"
    reconfig = False
    
    def __init__(self,configdir,jsonblob={}):
        super(PluginConfig,self).__init__(configdir,jsonblob)
        self.modules = []
        if "modules" in jsonblob:
            if not isiterable(jsonblob["modules"]):
                raise ConfigError("modules field must be a list")
            for mod in jsonblob["modules"]:
                self.modules.append(importlib.import_module(mod))

@global_config_section
class LogConfig(ConfigSection):
    name = "log"
    reconfig = True

    def __init__(self,configdir,jsonblob={}):
        super(LogConfig,self).__init__(configdir,jsonblob)
        if "stderr" in jsonblob:
            self.stderr = bool(jsonblob["stderr"])
        else:
            self.stderr = False
        if "syslog" in jsonblob:
            self.syslog = jsonblob["syslog"]
        else:
            self.syslog = "linkmanager"
        if "file" in jsonblob:
            self.file = jsonblob["file"]
            if not os.access(self.file,os.W_OK):
                raise ConfigError("cannot write to log file '%s'" % (self.file,))
        else:
            self.file = None
        if "level" in jsonblob:
            l = jsonblob["level"]
            if l in ("error","ERROR"):
                self.level = logging.ERROR
            elif l in ("warning","WARNING"):
                self.level = logging.WARNING
            elif l in ("info","INFO"):
                self.level = logging.INFO
            elif l in ("debug","DEBUG"):
                self.level = logging.DEBUG
            else:
                raise ConfigError("unknown log level '%s'; must be one of" \
                                  " error,warning,info,debug" % (self.level))
        else:
            self.level = logging.WARNING
        if "other_level" in jsonblob:
            l = jsonblob["other_level"]
            if l in ("error","ERROR"):
                self.other_level = logging.ERROR
            elif l in ("warning","WARNING"):
                self.other_level = logging.WARNING
            elif l in ("info","INFO"):
                self.other_level = logging.INFO
            elif l in ("debug","DEBUG"):
                self.other_level = logging.DEBUG
            else:
                raise ConfigError("unknown log other_level '%s'; must be one of" \
                                  " error,warn,info,debug" % (self.other_level))
        else:
            self.other_level = logging.WARNING

    def __eq__(self,other):
        if type(self) == type(other) \
            and self.syslog == other.syslog \
            and self.file == other.file \
            and self.level == other.level \
            and self.other_level == other.other_level:
            return True
        return False

    def apply(self,manager,oldsection=None,oldobjects=None):
        other_modules = []
        procname = os.path.basename(sys.argv[0])
        if oldobjects is not None:
            state = oldobjects
        else:
            state = {}
        if (oldsection and self.syslog != oldsection.syslog) or self.syslog:
            nh = None
            if self.syslog:
                if isinstance(self.syslog,bool):
                    nh = logging.handlers.SysLogHandler(address='/dev/log')
                else:
                    nh = logging.handlers.SysLogHandler(
                        address='/dev/log',facility=self.syslog)
                fm = logging.Formatter(
                    procname+"[%(process)d] %(name)s %(levelname)s %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in other_modules:
                    logging.getLogger(mod).addHandler(nh)
            if oldsection:
                LOG.removeHandler(state["syslog_h"])
                for mod in other_modules:
                    logging.getLogger(mod).removeHandler(state["syslog_h"])
                del state["syslog_h"]
            if nh:
                state["syslog_h"] = nh
        if (oldsection and self.file != oldsection.file) or self.file:
            nh = None
            if self.file:
                nh = logging.FileHandler(self.file,mode='a')
                fm = logging.Formatter(
                    "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                    ":%(lineno)d %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in other_modules:
                    logging.getLogger(mod).addHandler(nh)
            if oldsection:
                LOG.removeHandler(state["file_h"])
                for mod in other_modules:
                    logging.getLogger(mod).removeHandler(state["file_h"])
                del state["file_h"]
            if nh:
                state["file_h"] = nh
        if self.stderr or not self.file:
            # Once we open a console logger, it stays forever.
            if "console_h" not in state:
                nh = logging.StreamHandler()
                fm = logging.Formatter(
                    "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                    ":%(lineno)d %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in other_modules:
                    logging.getLogger(mod).addHandler(nh)
                state["console_h"] = nh
        LOG.setLevel(self.level)
        for mod in other_modules:
            logging.getLogger(mod).setLevel(self.other_level)

        # Return the (possibly modified) state.
        return state

@global_config_section
class ManagerConfig(ConfigSection):
    name = "manager"
    reconfig = False
    
    def __init__(self,configdir,jsonblob={}):
        super(ManagerConfig,self).__init__(configdir,jsonblob)
        self.impotent = False
        if "impotent" in jsonblob:
            if not isinstance(jsonblob["impotent"],bool):
                raise ConfigError("impotent field must be true or false")
            self.impotent = jsonblob["impotent"]
        self.type = None
        if "type" in jsonblob:
            if not isinstance(jsonblob["type"],str):
                raise ConfigError("type field must be string")
            self.type = jsonblob["type"]
        if "defroute_priority" in jsonblob:
            if not isinstance(jsonblob["defroute_priority"],int):
                raise ConfigError("defroute_priority must be a integer > 0")
            self.defroute_priority = int(jsonblob["defroute_priority"])
            if self.defroute_priority <= 0:
                raise ConfigError("defroute_priority must be > 0")
        else:
            self.defroute_priority = 1
        if "min_change_interval" in jsonblob:
            if not isinstance(jsonblob["min_change_interval"],int):
                raise ConfigError("min_change_interval must be a integer >= 0")
            self.min_change_interval = int(jsonblob["min_change_interval"])
            if self.min_change_interval <= 0:
                raise ConfigError("min_change_interval must be >= 0")
        else:
            self.min_change_interval = 60
        if "min_interface_stable_up_time" in jsonblob:
            if not isinstance(jsonblob["min_interface_stable_up_time"],int):
                raise ConfigError(
                    "min_interface_stable_up_time must be a integer >= 0")
            self.min_interface_stable_up_time = \
              int(jsonblob["min_interface_stable_up_time"])
            if self.min_interface_stable_up_time <= 0:
                raise ConfigError("min_interface_stable_up_time must be >= 0")
        else:
            self.min_interface_stable_up_time = 60
        if "families" in jsonblob:
            fl = jsonblob["families"]
            if not isinstance(fl,list):
                fl = [ fl ]
            for f in fl:
                if not f in ("inet"):
                    raise ConfigError("'inet' is the only supported protocol family")
                self.families = [ socket.AF_INET ]
        else:
            self.families = [ socket.AF_INET ]
