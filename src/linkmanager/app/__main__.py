
import sys
import signal
import logging
from linkmanager.config import Config
from linkmanager.manager import Manager
from linkmanager.log import setLogger

def main():
    logging.basicConfig(stream=sys.stderr,level=logging.DEBUG)
    logger = logging.getLogger()
    setLogger(logger)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    try:
        import urllib3
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    except ModuleNotFoundError:
        pass
    config = Config()
    manager = Manager.from_config(config)
    def sigh(signum,frame):
        manager.stop()
        manager.cleanup()
        exit(0)
    for sig in [ signal.SIGHUP,signal.SIGINT,signal.SIGTERM ]:
        signal.signal(sig,sigh)
    ret = 0
    try:
        ret = manager.run()
    except:
        logger.exception("unhandled exception in manager; cleaning up and aborting")
    manager.cleanup()
    exit(ret)

if __name__ == "__main__":
    main()
