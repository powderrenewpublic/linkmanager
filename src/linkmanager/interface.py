
import time
import subprocess
import abc
import six
from linkmanager.log import LOG
from linkmanager.config import (
    global_config_section, ConfigSection, ConfigError )
#from linkmanager.service import SystemdServiceManager

class Interface(object):
    TYPE_ETHERNET = 0
    TYPE_WIFI = 1
    TYPE_LTE = 2
    TYPE_VPN = 3

    """
    """
    def __init__(self,type,name,priority,address=None,
                 l3manager=None,l2manager=None,
                 operstate_unknown_is_up=False):
        self.type = type
        self.name = name
        self.index = None
        self.priority = priority
        self.l3manager = l3manager
        self.l2manager = l2manager
        self.operstate_unknown_is_up = operstate_unknown_is_up
        self.carrier = 0
        self.operstate = "DOWN"
        self.address = address
        self.last_carrier_timestamp = time.time()
        self.last_state_timestamp = time.time()

    def reset(self):
        self.index = None
        self.carrier = 0
        self.operstate = "DOWN"
        self.address = None
        self.last_carrier_timestamp = time.time()
        self.last_state_timestamp = time.time()

    def clone(self):
        ret = Interface(
            self.type,self.name,self.priority,self.address,
            self.l3manager,self.l2manager,self.operstate_unknown_is_up)
        ret.carrier = self.carrier
        ret.operstate = self.operstate
        ret.last_carrier_timestamp = self.last_carrier_timestamp
        ret.last_state_timestamp = self.last_state_timestamp
        return ret

    def __repr__(self):
        return "Interface(%s,%r,%d,%r,%r,%r)" \
          % (self.name,self.index,self.priority,self.carrier,self.operstate,
             self.address)

    @staticmethod
    def from_config(configdict):
        (t,n) = (configdict["type"],configdict["name"])
        if t in ("wired","ethernet"):
            t = Interface.TYPE_ETHERNET
        elif t == "wifi":
            t = Interface.TYPE_WIFI
        elif t == "lte":
            t = Interface.TYPE_LTE
        elif t == "vpn":
            t = Interface.TYPE_VPN
        else:
            raise ConfigError("no type specified for interface %s" % (n,))
        if "priority" in configdict:
            p = int(configdict["priority"])
            if p <= 0:
                raise ConfigError("interface %s priority must be > 0" % (n,))
        else:
            p = 1
        (l2m,l3m) = (None,None)
        if "l2manager" in configdict:
            if configdict["l2manager"] == "wpa_supplicant":
                l2m = None
            elif configdict["l2manager"] == "mbim-networkd":
                l2m = None
            else:
                LOG.warn("unknown l2manager for %s",n)
        if "l3manager" in configdict:
            if configdict["l3manager"] == "systemd-networkd":
                l3m = SystemdNetworkdInterfaceManager()
            elif configdict["l2manager"] == "mbim-networkd":
                l2m = None
            else:
                LOG.warn("unknown l2manager for %s",n)
        if "operstate_unknown_is_up" in configdict:
            ouiu = bool(configdict["operstate_unknown_is_up"])
        else:
            ouiu = False
        return Interface(t,n,p,l2manager=l2m,l3manager=l3m,
                         operstate_unknown_is_up=ouiu)

    def exists(self):
        return bool(self.index is not None)

    def isup(self):
        return bool(
            self.exists()
            and (self.carrier == 1
                 and (self.operstate == "UP"
                      or (self.operstate_unknown_is_up
                          and self.operstate == "UNKNOWN"))))

    def uptime(self):
        if not self.isup():
            return 0
        t = time.time()
        return min(t - self.last_carrier_timestamp,
                   t - self.last_state_timestamp)

    def set_index(self,index):
        self.index = index
        if index is None:
            self.carrier = 0
            self.operstate = "DOWN"

    def set_operstate(self,operstate):
        if self.operstate_unknown_is_up and operstate == "UNKNOWN":
            operstate = "UP"
        if self.operstate != operstate:
            self.last_state_timestamp = time.time()
            self.operstate = operstate

    def set_carrier(self,carrier):
        if self.carrier != carrier:
            self.last_carrier_timestamp = time.time()
            self.carrier = carrier

    def set_address(self,address):
        if self.address != address:
            self.address = address

@six.add_metaclass(abc.ABCMeta)
class InterfaceManager(object):
    """
    Manages an interface.
    """
    def __init__(self):
        pass

    @abc.abstractmethod
    def restart(self,interface,wait=False):
        pass

    @abc.abstractmethod
    def restartAll(self,wait=False):
        pass

class SystemdNetworkdInterfaceManager(InterfaceManager):
    def __init__(self):
        super(SystemdNetworkdInterfaceManager,self).__init__()

    def restart(self,interface,wait=False):
        m = linkmanager.service.Factory.getInstance('systemd')
        ret = m.restart('systemd-networkd.service',wait=wait)
        return ret

    def restartAll(self,wait=False):
        return self.restart()

class IfUpDownInterfaceManager(InterfaceManager):
    def __init__(self):
        super(IfUpDownInterfaceManager,self).__init__()

    def restart(self,interface,wait=False):
        cmd = "ifdown %s && ifup %s" % (interface.name,interface.name)
        if wait:
            ret = subprocess.call(cmd,shell=True)
        else:
            ret = subprocess.Popen(cmd,shell=True)              
        return ret

    def restartAll(self,wait=False):
        interfaces = []
        for ifacename in subprocess.check_output("ifquery -l").split("\n"):
            interfaces.append(ifacename)
        ret = 0
        for ifacename in interfaces:
            ret |= self.restart(ifacename,wait=wait)
        return ret

# XXX: replace with DBUS API.
class NMInterfaceManager(InterfaceManager):
    def __init__(self,interfaces):
        super(NMInterfaceManager,self).__init__(interfaces)

    def restart(self,interface,wait=False):
        cmd = "nmcli connection down %s && nmcli connection up %s" \
            % (interface.name,interface.name)
        if wait:
            ret = subprocess.call(cmd,shell=True)
        else:
            ret = subprocess.Popen(cmd,shell=True)              
        return ret

    def restartAll(self,wait=False):
        cmd = "nmcli networking off && nmcli networking on"
        if wait:
            ret = subprocess.call(cmd,shell=True)
        else:
            ret = subprocess.Popen(cmd,shell=True)              
        return ret

@global_config_section
class InterfaceConfig(ConfigSection):
    name = "interfaces"
    reconfig = False
    
    def __init__(self,configdir,jsonblob={}):
        super(InterfaceConfig,self).__init__(configdir,jsonblob)
        self.interfaces = dict()
        if not isinstance(jsonblob,dict):
            raise ConfigError("interfaces must be a dict")
        for (name,ifconfig) in jsonblob.items():
            LOG.info("%s %s",str(name),str(ifconfig))
            ifconfig["name"] = name
            if not "type" in ifconfig:
                raise ConfigError("missing type key/value for interface %s"
                                  % (name))
            self.interfaces[name] = Interface.from_config(ifconfig)
