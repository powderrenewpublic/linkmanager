
import abc
import six

@six.add_metaclass(abc.ABCMeta)
class Stat(object):

    @abc.abstractmethod
    def query(self,iface,start,end):
        pass

class Result(object):
    pass
    
class LossResult(Result):
    def __init__(self,loss,iface,start,end):
        self.loss = loss
        self.iface = iface
        self.start = start
        self.end = end

    def __repr__(self):
        return "LossResult(%f,%r,%f,%f,%f)" \
          % (self.loss,self.iface,self.start,self.end,self.end - self.start)
