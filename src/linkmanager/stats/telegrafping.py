
from influxdb import InfluxDBClient
from linkmanager.stats import (Stat,LossResult)
from linkmanager.config import ConfigError
from linkmanager.log import LOG

class TelegrafPingStat(Stat):
    def __init__(self,name,database,measurement,
                 hostname="localhost",port=8086,username="",password=""):
        self.name = name
        self.database = database
        self.measurement = measurement
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password

    @staticmethod
    def fromConfig(kls,configDict):
        d = dict(configDict)
        if "name" not in d:
            name = kls.__name__
        else:
            name = d["name"]
            del d["name"]
        if "database" in d:
            db = d["database"]
            del d["database"]
        else:
            raise ConfigError("%s probe %s: no database key/value defined"
                              % (kls.__name__,name))
        if "measurement" in d:
            meas = d["measurement"]
            del d["measurement"]
        else:
            raise ConfigError("%s probe %s: no measurement key/value defined"
                              % (kls.__name__,name))
        for k in d:
            if not k in ["hostname","port","username","password"]:
                raise ConfigError(
                    "%s probe %s: unknown key %s" % (kls.__name__,name,k))
        return TelegrafPingStat(name,db,meas,**d)

    def query(self,iface,start,end):
        startns = start * 1000000000
        endns = end * 1000000000
        ret = []
        client = InfluxDBClient(
            self.hostname,self.port,self.username,self.password,self.database)
        res = client.query(
            "select count(result_code) as count,sum(packets_received) as recv,sum(packets_transmitted) as trans,mean(average_response_ms) as rtt,max(maximum_response_ms) as rtt_max,min(minimum_response_ms) as rtt_min,mean(standard_deviation_ms) as rtt_stddev from %s where interface='%s' and time >= %d and time < %d;"
            % (self.measurement,iface.name,int(startns),int(endns)))
        count = 0
        if res is not None and list(res.get_points()):
            d = list(res.get_points())[0]
            (count,recv,trans,rtt,rtt_max,rtt_min,rtt_stddev) = \
                (d["count"],d["recv"],d["trans"],d["rtt"],d["rtt_max"],d["rtt_min"],d["rtt_stddev"])
            # Sometimes not all values are there.
            if count is None or recv is None or trans is None:
                LOG.info("some telegraf values were null (%r,%r,%r); aborting query",
                         count, recv, trans)
                return ret
            # Seems like telegraf agent must have a bug where recv can be >
            # trans.  Not much else we can do.
            if recv > trans:
                trans = recv
            loss = 1 - float(recv)/trans
            if count > 0:
                res2 = client.query(
                    "select count(result_code) as count_failed from %s where result_code != 0 and interface='%s' and time >= %d and time < %d;"
                    % (self.measurement,iface.name,int(startns),int(endns)))
                if res2 is not None and list(res2.get_points()):
                    count_failed = list(res2.get_points())[0]["count_failed"]
                else:
                    count_failed = 0
                # Ensure value is not None.
                if count_failed is None:
                    count_failed = 0
                floss = float(count_failed)/count
                ret.append(LossResult(loss+floss,iface,start,end))
        return ret
                

                    
                
