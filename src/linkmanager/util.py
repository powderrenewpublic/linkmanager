from argparse import Namespace

class DictNamespace(dict,Namespace):
    """
    A simple class that is similar to argparse.Namespace, but is also an
    iterable dictionary, and allows key/value deletion.
    """
    def __setattr__(self,attr,value):
        self.__setitem__(attr,value)

    def __getattr__(self,attr):
        if attr in self.keys():
            return self.__getitem__(attr)
        return dict.__getattribute__(self,attr)

    def __delattr__(self,attr):
        if attr in self.keys():
            return self.__delitem__(attr)
        return dict.__delattr__(self,attr)

def isiterable(x):
    """
    :param x: any Python value.
    :returns: True if ``x`` is iterable; False otherwise.
    """
    try:
        iter(x)
        return True
    except TypeError:
        return False
