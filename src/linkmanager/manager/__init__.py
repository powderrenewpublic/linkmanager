import abc
import select
import atexit
import traceback
import socket
import time
import sys
import heapq
import six
from linkmanager.config import ConfigError
from linkmanager.log import LOG
from linkmanager.exceptions import LinkManagerError
from linkmanager.netlink import (
    RtNetlink,NetlinkMsg,LinkMsg,AddrMsg,RouteMsg)
import linkmanager.interface
from linkmanager.event.log import EventLog
from linkmanager.event import (
    InterfaceCarrierEvent,InterfaceOperStateEvent,InterfaceLoadEvent,
    InterfaceNewEvent,InterfaceDelEvent,
    AddressLoadEvent,AddressNewEvent,AddressDelEvent,
    RouteLoadEvent,RouteNewEvent,RouteDelEvent,
    DefRouteNewEvent,DefRouteUpdateEvent,DefRouteChangeEvent,DefRouteDelEvent )

DEFAULT_MANAGER = "netlink"

manager = None
def getGlobalManager():
    return manager
def setGlobalManager(m):
    global manager
    manager = m

class Task(object):
    RUNNABLE = 1
    CANCELED = 0

    def __init__(self,item,timestamp):
        self.state = Task.RUNNABLE
        self.item = item
        self.timestamp = timestamp

    def cancel(self):
        self.state = Task.CANCELED

    def isrunnable(self):
        return self.state == Task.RUNNABLE

class Schedule(object):
    def __init__(self):
        self.schedule = []

    def add(self,task):
        heapq.heappush(self.schedule,(task.timestamp,task))

    def next(self):
        while self.schedule \
          and self.schedule[0].timestamp <= time.time():
            t = heapq.heappop(self.schedule)
            if t.isrunnable():
                return t
        return None

@six.add_metaclass(abc.ABCMeta)
class Manager(object):
    """
    A Manager is the top-level object that arbitrates the event loop,
    detects the state of the world, manages the action queue, and drives
    interfaces into the right state by prescribing corrective actions
    and observing their effect.
    """
    def __init__(self,config):
        self._cancel = False
        self.config = config
        self.schedule = Schedule()
        self.interfaces = dict(config.interfaces.interfaces)
        self.task_fds = dict()
        self.eventlog = EventLog.from_config(config.eventlog)

    @abc.abstractmethod
    def run(self):
        pass

    def stop(self):
        self._cancel = True

    def cleanup(self):
        if self.eventlog:
            self.eventlog.close()

    @staticmethod
    def from_config(config):
        global manager
        mtype = DEFAULT_MANAGER
        m = None
        if config.manager.type:
            mtype = config.manager.type
        if mtype == "netlink":
            m = NetlinkManager(config)
        elif mtype == "dbus":
            m = DBusManager(config)
        else:
            LOG.warn("unknown manager %s",mtype)
        if manager is None:
            setGlobalManager(m)
        return m

    def calculate(self):
        lowest = None
        for (name,iface) in self.interfaces.items():
            isup = iface.isup()
            uptime = iface.uptime()
            if not isup \
              or uptime < self.config.manager.min_interface_stable_up_time:
                LOG.debug("not considering %s (up=%s,uptime=%f)",
                          name,iface.isup(),iface.uptime())
                continue
            if lowest is None or iface.priority < lowest.priority:
                lowest = iface
        LOG.debug("lowest-cost interface %r",lowest)
        return lowest

class NetlinkManager(Manager):
    def __init__(self,config):
        super(NetlinkManager,self).__init__(config)
        self.rtnl = RtNetlink()
        self.rtnl.listen()
        self.interface_index_map = dict()
        self.our_defroute = dict()
        self.our_iface = None
        self.iface_defroutes = dict()
        self.last_change_timestamp = 0
        self.__last_stats_zero = False

    def cleanup(self):
        ret = True
        if self.our_defroute is None or not self.our_defroute:
            LOG.info("no default route set; nothing to clean up.")
        else:
            LOG.info("cleanup removing default route: %s",str(self.our_defroute))
            try:
                ret = None
                if not self.config.manager.impotent:
                    ret = self.rtnl.rtnl.get_routes(**self.our_defroute)
                if ret:
                    self.rtnl.rtnl.route("del",**self.our_defroute)
                self.our_defroute = dict()
                self.our_iface = None
                self.last_change_timestamp = 0
            except Exception:
                LOG.error("error removing route during cleanup",
                          exc_info=sys.exc_info())
                ret = False
        super(NetlinkManager,self).cleanup()
        return ret

    def update_defroute(self,iface,**kwargs):
        if self.our_defroute:
            LOG.info("removing old defroute on %s (%s)",
                     self.our_iface,str(self.our_defroute))
            try:
                ret = None
                if not self.config.manager.impotent:
                    ret = self.rtnl.rtnl.get_routes(**self.our_defroute)
                if ret:
                    self.rtnl.rtnl.route("del",**self.our_defroute)
                self.our_defroute = dict()
                self.our_iface = None
            except Exception:
                LOG.error("error removing route during update",
                          exc_info=sys.exc_info())
                return False
        if kwargs:
            LOG.info("adding new defroute on %s (%s)",iface,str(kwargs))
            try:
                if not self.config.manager.impotent:
                    self.rtnl.rtnl.route("add",**kwargs)
                self.our_defroute = dict(kwargs)
                self.our_iface = iface
                self.last_change_timestamp = time.time()
            except Exception:
                LOG.error("error adding route during update",
                          exc_info=sys.exc_info())
                return False
        return True

    #def rtnl_handler(self,iochan,condition):
    #    self.rtnl.handler()
    #    #loop.quit()
    #    #ioc.add_watch(GLib.IO_IN,rtnl_handler,priority=GLib.PRIORITY_DEFAULT)
    #    return True

    def log_events(self,events):
        self.eventlog.log(events)

    def load_ifaces(self):
        events = []
        links = self.rtnl.get_links()
        for l in links:
            LOG.debug("link: %s",l.attr("ifname"))
            if l.attr("ifname") in self.interfaces:
                name = l.attr("ifname")
                self.interfaces[name].set_index(l.index)
                self.interfaces[name].set_operstate(l.attr("operstate"))
                self.interfaces[name].set_carrier(l.attr("carrier"))
                self.interfaces[name].set_address(l.attr("address"))
                self.interface_index_map[l.index] = name
                LOG.debug("interface %s updated: %s",
                          name,self.interfaces[name])
                events.append(InterfaceLoadEvent(self.interfaces[name]))
        LOG.debug("interfaces: %s",str(self.interfaces))
        addrs = self.rtnl.get_addrs()
        for l in addrs:
            if l.index in self.interface_index_map:
                name = self.interface_index_map[l.index]
                addr = l.attr("address")
                if not l.family in self.config.manager.families:
                    continue
                LOG.debug("address %s/%r on %s",addr,l.prefixlen,name)
                events.append(AddressLoadEvent(name,l.family,addr,l.prefixlen))
        # Second, grab the current default routes.  Check to see if we
        # have set ours yet, and clear our state if we don't find ours.
        # Find the per-interface default routes too.
        routes = self.rtnl.get_routes(
            dst_len=0,table=254)
        for (name,iface) in self.interfaces.items():
            for r in routes:
                if not r.family in self.config.manager.families:
                    continue
                if iface.index == r.attr("oif"):
                    self.iface_defroutes[iface.name] = r
                    rd = r.toNetlinkDict()
                    rd["interface"] = name
                    events.append(RouteLoadEvent(rd))
        LOG.debug("iface_defroutes: %s",str(self.iface_defroutes))
        self.log_events(events)

    def calculate(self):
        t = time.time()
        ifres = dict()
        from linkmanager.stats.telegrafping import TelegrafPingStat
        s = TelegrafPingStat("ping","telegraf","ping")
        allzero = True
        for (name,iface) in self.interfaces.items():
            isup = iface.isup()
            uptime = iface.uptime()
            if not isup \
              or uptime < self.config.manager.min_interface_stable_up_time:
                LOG.debug("not considering %s (up=%s,uptime=%f)",
                          iface.name,iface.isup(),iface.uptime())
                continue
            elif not name in self.iface_defroutes:
                LOG.debug("not considering %s (up=%s,uptime=%f); no defroute!",
                          iface.name,iface.isup(),iface.uptime())
                continue
            start = t - min(uptime,5*60)
            qres = s.query(iface,start,t)
            if not qres:
                continue
            ifres[name] = qres[0]
            if ifres[name].loss != 0.0:
                allzero = False
        if ifres and not (self.__last_stats_zero and allzero):
            LOG.info("results: %s",
                     ",".join(["%s(loss=%f,up=%s,uptime=%f)" % \
                                   (res.iface.name,res.loss,res.iface.isup(),res.iface.uptime())
                                   for res in ifres.values()]))
        if ifres:
            self.__last_stats_zero = allzero
        else:
            self.__last_stats_zero = False
        lowest_value = 0.0
        lowest = None
        for (name,res) in ifres.items():
            iface = self.interfaces[name]
            if not lowest or res.loss < lowest_value \
              or (res.loss == lowest_value and lowest.priority > iface.priority):
                lowest = iface
                lowest_value = res.loss
        return lowest

    def run(self):
        self._cancel = False

        # Ensure we have the correct ifindex numbers for each of our
        # named interfaces, and defroutes for them.
        self.load_ifaces()

        # Run our loop to handle netlink events, run probes, etc.  It
        # updates interface and defroute state via netlink events.
        last_calculate = 0
        while True:
            if self._cancel:
                return 0

            rfds = []
            try:
                rfds = select.select(
                    [self.rtnl.fileno()],[],[],1.0)[0]
            except select.error:
                if self._cancel:
                    return 0
                LOG.debug("error in select",exc_info=sys.exc_info())
                continue
            et = time.time()
            event_recompute = False
            events = []
            if self.rtnl.fileno() in rfds:
                msglist = self.rtnl.handler()
                for msg in msglist:
                    if isinstance(msg,(LinkMsg,AddrMsg,RouteMsg)):
                        if isinstance(msg,(AddrMsg,RouteMsg)) \
                          and not msg.family in self.config.manager.families:
                            continue
                        #LOG.debug(msg)
                        for x in msg.dumps(skipattrslike=["stat","spec"]):
                            LOG.debug(x)
                    # Check for a link changing carrier or operstate
                    # XXX: need to handle new/delete and new index.
                    if isinstance(msg,LinkMsg):
                        name = msg.attr("ifname")
                        operstate = msg.attr("operstate")
                        carrier = msg.attr("carrier")
                        index = msg.index
                        if msg.attr("wireless") and operstate is None \
                          and carrier is None:
                            LOG.info("ignoring interface %s wireless msg",name)
                            continue
                        if msg.nltype == NetlinkMsg.types.RTM_NEWLINK \
                          and name in self.interfaces:
                            iface = self.interfaces[name]
                            # Did our interface change ifindex?
                            if index != iface.index:
                                LOG.info("interface %s changed index: %r -> %r",
                                         name,iface.index,index)
                                oldindex = iface.index
                                if oldindex in self.interface_index_map:
                                    del self.interface_index_map[oldindex]
                                self.interface_index_map[index] = iface.name
                                iface.set_index(index)
                                event_recompute = True
                                events.append(InterfaceNewEvent(
                                    iface))
                            # Did our interface change state?
                            if iface.operstate != operstate:
                                LOG.info("%r changed to operstate %s",
                                         iface,str(operstate))
                                iface.set_operstate(operstate)
                                event_recompute = True
                                events.append(InterfaceOperStateEvent(
                                    iface))
                            # Did our interface change carrier?
                            if iface.carrier != carrier:
                                LOG.info("%r changed carrier to %s",
                                         iface,str(carrier))
                                iface.set_carrier(carrier)
                                event_recompute = True
                                events.append(InterfaceCarrierEvent(
                                    iface))
                            # Is our interface down?
                            if not iface.isup():
                                # Remove the defroute for this iface if
                                # it goes down to prevent calculate from
                                # choosing it even when it comes up,
                                # until the routes are set for it!
                                # XXX: is this right?  Can't we clear this
                                # when the backing route drops?
                                if name in self.iface_defroutes:
                                    del self.iface_defroutes[name]
                        elif msg.nltype == NetlinkMsg.types.RTM_DELLINK \
                          and name in self.interfaces:
                            LOG.info("interface %s deleted",name)
                            iface = self.interfaces[name]
                            oldindex = iface.index
                            oldiface = iface.clone()
                            events.append(InterfaceDelEvent(oldiface))
                            if index in self.interface_index_map:
                                del self.interface_index_map[oldindex]
                            iface.reset()
                            if name in self.iface_defroutes:
                                del self.iface_defroutes[name]
                                LOG.info("removed stale interface defroute for deleted %s",
                                         name)
                            if name == self.our_iface:
                                LOG.info("defroute interface deleted; removing defroute %s",
                                         str(self.our_defroute))
                                event_recompute = True
                                events.append(DefRouteDelEvent(
                                    dict(self.our_defroute),timestamp=et))
                                self.update_defroute(self.our_iface)
                    if isinstance(msg,AddrMsg):
                        name = msg.attr("label")
                        if not msg.index in self.interface_index_map:
                            continue
                        #if not name in self.interfaces:
                        #    continue
                        name = self.interface_index_map[msg.index]
                        addr = msg.attr("address")
                        opstr = None
                        if msg.nltype == NetlinkMsg.types.RTM_NEWADDR:
                            opstr = "added"
                            events.append(AddressNewEvent(
                                name,msg.family,addr,msg.prefixlen,
                                timestamp=et))
                        elif msg.nltype == NetlinkMsg.types.RTM_DELADDR:
                            opstr = "removed"
                            events.append(AddressDelEvent(
                                name,msg.family,addr,msg.prefixlen,
                                timestamp=et))
                        if opstr is not None and addr is not None:
                            LOG.info("%r %s address %s",
                                     self.interfaces[name],opstr,addr)
                    if isinstance(msg,RouteMsg) \
                      and msg.table == 254 and msg.dst_len == 0:
                        oif = msg.attr("oif")
                        if not oif in self.interface_index_map:
                            continue
                        name = self.interface_index_map[oif]
                        iface = self.interfaces[name]
                        opstr = None
                        if msg.nltype == NetlinkMsg.types.RTM_NEWROUTE:
                            opstr = "added"
                            if name == self.our_iface:
                                # Check to see if our copy of the route
                                # is identical to the one we're receiving;
                                # we might have caused this NEWROUTE msg.
                                nd = msg.toNetlinkDict()
                                if self.our_defroute != nd:
                                    # Our defroute iface might have
                                    # changed addresses or gateways;
                                    # update our higher-prio copy if so.
                                    LOG.info(
                                        "updating defroute on %r to %s (%s)",
                                        self.interfaces[name],str(nd),
                                        str(self.our_defroute))
                                    nd['priority'] = self.config.manager.defroute_priority
                                    self.update_defroute(iface,**nd)
                                    nd["interface"] = name
                                    events.append(DefRouteUpdateEvent(
                                        nd,timestamp=et))
                            else:
                                # Update our cache of defroutes
                                self.iface_defroutes[name] = msg
                                LOG.debug("updated %r defroute (%r)",
                                          iface,msg)
                                nd = msg.toNetlinkDict()
                                nd["interface"] = name
                                events.append(RouteNewEvent(
                                    nd,timestamp=et))
                        elif msg.nltype == NetlinkMsg.types.RTM_DELROUTE:
                            opstr = "removed"
                            if name == self.our_iface:
                                rd = dict(self.our_defroute)
                                rd["interface"] = name
                                events.append(DefRouteDelEvent(
                                    rd,timestamp=et))
                                event_recompute = True
                                self.update_defroute(iface)
                            nd = msg.toNetlinkDict()
                            nd["interface"] = name
                            events.append(RouteDelEvent(nd,timestamp=et))

            #
            # Should we recalculate, and possibly replace, routes?
            #
            if event_recompute \
              or ((time.time() - self.last_change_timestamp) \
                  > self.config.manager.min_change_interval \
                  and (time.time() - last_calculate > 10.0)):
                iface = self.calculate()
                if iface:
                    if iface != self.our_iface:
                        LOG.info("changing defroute from %r to %r",
                                 self.our_iface,iface)
                        nd = self.iface_defroutes[iface.name].toNetlinkDict()
                        nd["priority"] = self.config.manager.defroute_priority
                        oldinterface = self.our_iface
                        self.update_defroute(iface,**nd)
                        nd["interface"] = iface.name
                        if oldinterface:
                            nd["oldinterface"] = oldinterface.name
                            events.append(DefRouteChangeEvent(nd,timestamp=et))
                        else:
                            events.append(DefRouteNewEvent(nd,timestamp=et))

                    last_calculate = time.time()
                else:
                    LOG.debug("no interface after calculate")

            self.log_events(events)


class DBusManager(Manager):
    def __init__(self,config):
        import dbus
        from gi.repository import GLib

        super(DBusManager,self).__init__(config)
        # Initialize our dbus loop.
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.loop = GLib.MainLoop()
        # Get a connection to the system dbus
        self.bus = dbus.SystemBus()#(mainloop=loop)
        self.bus.add_signal_receiver(
            self.default_signal_handler,interface_keyword='dbus_interface',
            member_keyword='member')
        # Listen for various signals on the system bus: systemd,
        # systemd-networkd, wpa_supplicant
        self.systemd = self.bus.get_object(
            'org.freedesktop.systemd1','/org/freedesktop/systemd1')
        self.systemd_manager = dbus.Interface(
            self.systemd,dbus_interface='org.freedesktop.systemd1.Manager')
        self.systemd_manager.Subscribe()
        self.wpa_supplicant = self.bus.get_object(
            'fi.w1.wpa_supplicant1','/fi/w1/wpa_supplicant1')
        self.modem_manager = self.bus.get_object(
            'org.freedesktop.ModemManager1','/org/freedesktop/ModemManager1')
        #self.wpa_supplicant.Subscribe()
        self.rtnl = RtNetlink()
        self.rtnl.listen()
        # Pass rtnl into glib
        self.rtnl_ioc = GLib.IOChannel.unix_new(self.rtnl.fileno())
        self.rtnl_ioc.set_encoding(None)
        self.rtnl_ioc.set_buffered(False)
        #ioc = GLib.IOChannel(rtnl._sock.fileno())
        self.rtnl_ioc.add_watch(GLib.IO_IN,self.rtnl_handler,
                                priority=GLib.PRIORITY_DEFAULT)

    def rtnl_handler(self,iochan,condition):
        self.rtnl.handler()
        #loop.quit()
        #ioc.add_watch(GLib.IO_IN,rtnl_handler,priority=GLib.PRIORITY_DEFAULT)
        return True

    def default_signal_handler(self,*args,**kwargs):
        LOG.debug("dbus: %s (%s)",str(args),str(kwargs))

    def run(self):
        self.loop.run()

    def halt(self):
        self.systemd_manager.Unsubscribe()
