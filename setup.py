#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
import os.path
import sys

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='linkmanager',
    version='0.3.3',
    author='David M Johnson',
    author_email='johnsond@flux.utah.edu',
    url='https://gitlab.flux.utah.edu/emulab/linkmanager',
    description='A link manager.  Arbitrates between differently-prioritized uplinks using heuristics; drives links into UP states; supports policy-driven link management.',
    long_description=long_description,
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: POSIX :: Linux',
        'Topic :: Internet',
        'Topic :: System :: Networking',
        'Topic :: System :: Networking :: Monitoring',
        'Topic :: System :: Systems Administration',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    keywords='link management network uplink',
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    #install_requires=['dbus','pyroute2',],
    #extra_requires=[],
    entry_points={
        'console_scripts': [
            'linkmanager=linkmanager.app.__main__:main',
        ],
    },
    #scripts=[ 'bin/linkmanager' ],
    #
    # Ok, for the same reason, we don't use console_scripts, we also
    # cannot use scripts.  Rather than implement our own version of
    # either, we just use data_files.  There are all kinds of reasons
    # not to do this, but it gets the job done for now, if you are
    # installing to the filesystem as a privileged user.  If you are
    # install --user, not so good for you!
    #
    #data_files=[(os.path.join(sys.exec_prefix,'bin'),['bin/linkmanager']),
    #            ('bin',['bin/linkmanager'])],
    #include_package_data=True,
)
