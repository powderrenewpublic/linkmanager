linkmanager
-------------

A link manager.  Arbitrates between differently-prioritized uplinks
using heuristics; drives links into UP states; supports policy-driven
link management.
